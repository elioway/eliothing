![](https://elioway.gitlab.io/eliothing/elio-Thing-logo.png)

> Dropping the pretence one thing is so different from another, **the elioWay**

# eliothing ![experimental](https://elioway.gitlab.io/eliosin/icon/devops/experimental/favicon.ico "experimental")

## The Law

1. As the data fields for every thing _engaged_, you will restrict yourself to the names of `Schema.org`'s core Properties

- For clarity: these are Property names found in `Thing`.

2. Let the `subjectOf` Property be used to store the id of, or a hard wired object like { name: "elioWay", "engage": ["ItemList", ""]} , which represents the `thing` this is subject to: its Parent.

3. You are allowed the special `engage` Property which will be a standard dictionary object.

- `engage` should have an `ItemList` object.
- `engage` could have a `Permit` object, holding any application credentials, or an engagable App with an ItemList of its own.
- `engage` could habe a `Date` object, with properties for createdDate, modifiedDate, etc.
- `engage` could habe a `Person` object, identofier the person owning or responsible for the data in the pther `engage` keys.

4. You are allowed the special `disengage` Property.

- It means there will always be a place to softly delete and undelete `engage` keys, by simply moving any between `engage` into `disengage` and back.
- Or list items by moving any `engage.ItemList.itemListElement` into `disengage.ItemList.itemListElement` and back.

5. Do customize your own Schemas called, I don't know, named, for example, something like **eliothing**. There is no need to restrict yourself to `Schema.org` Type names.

- Put its name in the `additionalType` field.

6. Things do not need unique identifiers.

- In an application context, the output of some function to return `listT` is probably best done by sensibly merging items which are indistinct to keep the list permanently down in size.
- In terms of data, think small. A `thing` is a record with a list. It doesn't share anything in that list with any other `thing` except by mutual agreement. People will store their own data, thank you very much. They just need you for the App. The App is everything from the data that the App needs...

7. As the data for everything _listed_, thou shalt restrict thyself to the core Properties of `Schema.org` (found in `Thing`).

8. You are allowed the special `engage` Property in `listT` output. The output will be the means to communicate meaning information bbout every item being listed, even if that is just a list of the `engage` keys in the _engaged_ version of the thing (were it to be iterated).

### Example `thing`

```json
{
  "additionalType": "elioFoodEstablishment",
  "alternateName": "FoodEstablishment Thing, **the elioWay**",
  "description": "Lengthy description field - the main textual content of this FoodEstablishment",
  "disambiguatingDescription": "FoodEstablishment App with both an indoor and delivery order system",
  "identifier": "`Things do not need unique identifiers`",
  "image": "meh.png",
  "mainEntityOfPage": "FoodEstablishment",
  "name": "elioFood",
  "potentialAction": "CompletedActionStatus when all orders have been delivered",
  "sameAs": "https://schema.org/FoodEstablishment",
  "subjectOf": { "name": "elioWay", "engage": ["ItemList", "elioWay"] },
  "url": "https://FoodEstablishment.theelioway.com",
  "FoodEstablishment": {
    "acceptsReservations": "",
    "hasMenu": "",
    "menu": [],
    "servesCuisine": "",
    "starRating": ""
  },
  "ItemList": {
    "itemListElement": [
      {
        "disambiguatingDescription": "Chilli encrusted, filled with fresh vegetables, mozzarella, and spinach.",
        "identifier": "curriedcalzones",
        "name": "Curried Calzones",
        "sameAs": "MenuItem",
        "subjectOf": "elioFood",
        "MenuItem": {
          "offers": 9.5,
          "suitableForDiet": "VegetarianDiet"
        }
      },
      {
        "disambiguatingDescription": "Made from a wide range of toppings including peas, spiced hard-boiled eggs, seafood and green olives.",
        "identifier": "spicysicilian",
        "name": "Spicy Sicilian",
        "sameAs": "MenuItem",
        "subjectOf": "elioFood",
        "MenuItem": {
          "offers": 4,
          "suitableForDiet": "MeatDiet"
        }
      },
      {
        "disambiguatingDescription": "Covered in caramelized onions, olive oil and fresh sliced chilli",
        "identifier": "fieryfocaccia",
        "name": "Fiery Focaccia",
        "sameAs": "MenuItem",
        "subjectOf": "elioFood",
        "MenuItem": {
          "offers": 2,
          "suitableForDiet": "VegetarianDiet"
        }
      },
      {
        "name": "Fiery Focaccia ",
        "potentialAction": "",
        "sameAs": "Order",
        "subjectOf": "elioFood",
        "url": "",
        "Order": {
          "customer": "BillHiggins",
          "orderDate": "2021-10-10 19:30",
          "orderedItem": "fieryfocaccia",
          "paymentDue": "2021-10-10 19:30",
          "seller": "elioFood"
        }
      }
    ],
    "numberOfItems": 4
  },
  "LocalBusiness": {
    "branchOf": "",
    "currenciesAccepted": "",
    "openingHours": "",
    "paymentAccepted": "",
    "priceRange": ""
  },
  "Organization": {
    "award": "",
    "awards": "",
    "contactPoints": ""
  },
  "Place": {
    "additionalProperty": "",
    "address": "",
    "latitude": "",
    "longitude": "",
    "reviews": [],
    "telephone": ""
  }
}
```

## Why are we using Schema.org?

Remember when Google drove cars to visually map every road it can? The elioWay's simple and repetive pattern is Google Maps for everything else.

<https://schema.org/QuantitativeValue> <https://schema.org/PropertyValue>

**eliothing** groups <https://schema.org> handling utilities, supporting server and client applications, **the elioWay**.

- [eliothing Documentation](https://elioway.gitlab.io/eliothing)

## Nutshell

- [eliothing Quickstart](https://elioway.gitlab.io/eliothing/quickstart.html)
- [eliothing Credits](https://elioway.gitlab.io/eliothing/credits.html)

![](https://elioway.gitlab.io/eliothing/apple-touch-icon.png)

## License

[MIT](LICENSE) [Tim Bushell](mailto:theElioWay@gmail.com)
