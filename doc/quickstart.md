# Quickstart eliothing

- [eliothing Prerequisites](/eliothing/prerequisites.html)
- [Installing eliothing](/eliothing/installing.html)

## Nutshell

- **eliothing** libraries resolve <https://schema.org> JSONLD into Models based on those _schemaTypes_.

## The ThingBuilder

The `ThingBuilder` is the basic class we're building.

- ThingBuilder should be constructed by passing in the JSONLD's `@graph` section.
- `schemify` method builds the schema into _schemaTypes_ groups.
- `modelMiner` method resolves the issue of having Fields with 1 or more _schemaTypes_ as Types.
- `modelMaker` method returns the definition of a single _schemaType_.
- `thing` method returns the definition _schemaType_ and all it's parent _schemaTypes_.

The **thing** library already encapsulates this functionality and outputs simple JSON objects.

Read the [thing Documentation][] for more details.

**mongoose-thing** and **django-thing** inherit from the ThingBuilder and override the `thing` method to return a definition which can be passed to the **MongooseJs** or **Django**.

## Creating a new eliothing library

1. Create a new app folder `your/repo/elioway/eliothing/thing-inMVCFramework`.
2. Create a `thing` method which returns a Model definition in the MVC Framework of your choice.
3. Get it working.
4. Test it.
5. Document it using [chisel](/elioangels/chisel).
6. Brand it using [generator-art](/elioangels/generator-art). Tell everyone about it.
7. Use it to do things **the elioWay**.
8. Push it to the <https://gitlab.com/eliothing/> group using [twig](/elioangels/twig).
