# Contributing

```shell
git clone https://gitlab.com/elioway/eliothing.git
cd eliothing
git submodule init
git submodule update
# or
set L thing django-thing ember-thing generator-thing k liar-thing mongoose-thing
for R in $L
    git submodule add git@gitlab.com:eliothing/$R.git
end
```

## TODOS

1. Take out the `engage` parent. It's unnecessary.
